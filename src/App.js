import React, { Component } from 'react';
import './index.css';
import Header from './components/header';
import Offers from './components/offers';
import Callback from './components/callback';
import LatestWorks from './components/latestWorks';
import Counters from './components/counters';
import News from './components/news';
import Form from './components/contactForm';
import Footer from './components/footer';

class App extends Component {
  render() {
    return (
      <div className="app">
        <Header/>
        <div class="content">
          <Offers/>
          <Callback/>
          <LatestWorks/>
          <Counters/>
          <News/>
          <Form/>
        </div>
        <Footer/>
      </div>
    );
  }
}

export default App;
