import React, { Component } from "react";
import "./style.css";
import Button from "../elements/button";

class Сallback extends Component {
  render() {
    return (
      <div className="section blue callback">
        <div class="container">
          <div class="callback__text">
            ХОТИТЕ НАЧАТЬ ЗАРАБАТЫВАТЬ В ИНТЕРНЕТЕ? ПРОСТО СВЯЖИТЕСЬ С НАМИ.
          </div>
          <Button link="#" text="Связаться" />
        </div>
      </div>
    );
  }
}

export default Сallback;
