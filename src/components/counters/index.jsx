import React, { Component } from "react";
import "./style.css";

class Counters extends Component {
  render() {
    const list = [
      { id: 1, title: "счастливых клиентов", counter: 456 },
      { id: 2, title: "проекта", counter: 322 },
      { id: 3, title: "сайтов в топ", counter: 290 },
      { id: 4, title: "сайта разработано", counter: 132 }
    ];

    return (
      <div className="section blue counters">
        <div className="container">
          <div class="counters-items">
            {list.map((el, index) => (
              <div key={el.id} class="counters-item">
                <div class="counters-item__number">{el.counter}</div>
                <div class="counters-item__title">{el.title}</div>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}

export default Counters;
