import React, { Component } from "react";
import "./style.css";

class Button extends Component {
  render() {
    return (
      <a className="btn" href={this.props.link}>
        {this.props.text}
      </a>
    );
  }
}

export default Button;
