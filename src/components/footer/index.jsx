import React, { Component } from "react";
import "./style.css";

import map from "../../assets/images/icons/placeholder.png";
import phone from "../../assets/images/icons/telephone.png";
import mail from "../../assets/images/icons/mail.png";

class Footer extends Component {
  render() {
    return (
      <footer className="footer">
        <div className="container">
          <div className="row">
            <div className="col-12 col-md-4 col-lg-3">
              <div className="footer__title">О нас</div>
              <div className="footer__about-text">
                Мы оказываем всестороннюю помощь компаниям и физическим лицам —
                собственникам веб-ресурсов, которые готовы использовать сайт,
                как эффективный рекламный инструмент, позволяющий развивать
                бизнес.
              </div>
            </div>
            <div className="col-12 col-md-4 col-lg-3 offset-lg-2">
              <div className="footer__title">Читайте в новостях</div>
              <a href="#">Новые разработки</a>
              <a href="#">Мы работаем, вы отдыхаете</a>
            </div>
            <div className="col-12 col-md-4 col-lg-3 offset-lg-1">
              <div className="footer__title">Контакты</div>
              <div className="text-icon">
                <img src={map} className="text-icon__img" />
                <span>Москва, Большая Спасская 12</span>
              </div>
              <div className="text-icon">
                <img src={phone} className="text-icon__img" />
                <span>8 (495) 626-46-00</span>
              </div>
              <div className="text-icon">
                <img src={mail} className="text-icon__img" />
                <span>info@gototop.com</span>
              </div>
            </div>
          </div>
        </div>
        <div class="copyright">2018 Все права защищены</div>
      </footer>
    );
  }
}

export default Footer;
