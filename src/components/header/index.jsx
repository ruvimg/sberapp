import React, { Component } from 'react';
import logo from '../../assets/images/icons/web.png';
import './style.css';
import NavBar from './navbar';
import Slider from './slider';

class Header extends Component {

  constructor(props) {
    super(props);
    this.state = {
      cssclass: '',
    };

    this.handleScroll = this.handleScroll.bind(this);
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll(event) {
    let scrollTop = window.scrollY;
    let tempclass = '';
    if(scrollTop > 200){
      tempclass = 'min'
    }
    if(this.state.cssclass !== tempclass){
      this.setState({
        cssclass: tempclass
      });
    }
   
  }

  render() {
    let headerClass = 'header ' + this.state.cssclass;
    return (
      <div className="top-content">
        <div className={headerClass}>
            <div className="container">
                <div className="logo">
                    <img src={logo} alt="logo" title="logo" />
                    <div className="logo__text">GO TO <span>TOP</span></div>
                </div>
                <NavBar/>
            </div>
        </div>
        <Slider/>
      </div>
    );
  }
}

export default Header;
