import React, { Component } from "react";
import "./style.css";

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cssclass: ""
    };

    this.activateMenu = this.activateMenu.bind(this);
  }

  activateMenu(e) {
    e.preventDefault();
    if (this.state.cssclass === "") {
      this.setState({
        cssclass: "active"
      });
    } else {
      this.setState({
        cssclass: ""
      });
    }
  }

  render() {
    const links = [
      { id: 1, title: "Главная", link: "#", active: 1 },
      { id: 2, title: "Новости", link: "#", active: 0 },
      { id: 3, title: "О компании", link: "#", active: 0 },
      { id: 4, title: "Контакты", link: "#", active: 0 }
    ];

    const content = links.map(link => (
      <li>
        <a
          className={link.active ? "active" : ""}
          href={link.link}
          key={link.id}
        >
          {link.title}
        </a>
      </li>
    ));

    const cssClassWrapper = "menu-wrapper " + this.state.cssclass;

    return (
      <div className={cssClassWrapper}>
        <div className="gamburger" onClick={this.activateMenu}>
          <span />
          <span />
          <span />
        </div>
        <ul className="top-menu">{content}</ul>
      </div>
    );
  }
}

export default NavBar;
