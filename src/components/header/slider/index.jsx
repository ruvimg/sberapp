import React, {Component} from 'react';
import './style.css';
import Swiper from 'swiper';
import 'swiper/dist/css/swiper.min.css'
import SliderImg from '../../../assets/images/backgrounds/slide.png';
import Button from './../../elements/button';

const Slide = function Slide(props) {
    return (
        <div className="main-slider-item" style={{ backgroundImage: `url(${props.slide.img})`}}>
            <div className="main-slider-item__content">
                <div className="main-slider-item__title">{props.slide.title}</div>
                <div className="main-slider-item__text">{props.slide.text}</div>
                <Button link={props.slide.link} text={props.slide.linkText}></Button>
            </div>
        </div>
    );
}

class Slider extends Component {
    constructor(props) {
        super(props);
        this.state = {
            slides: (function () {
                var slides = [
                    {   id: 1, 
                        title:"ВАШ САЙТ - ГЛАВНЫЙ БИЗНЕС-инструмент",
                        text:"GO TO TOP - Ваш быстрый старт продаж", 
                        linkText:"О нас", 
                        link:"#", 
                        img:SliderImg
                    },
                    {   id: 2, 
                        title:"ВАШ САЙТ - ГЛАВНЫЙ БИЗНЕС-инструмент 2",
                        text:"GO TO TOP - Ваш быстрый старт продаж 2", 
                        linkText:"О нас", 
                        link:"#", 
                        img:SliderImg
                    }
                ];
   
                return slides;
            }()),
            // virtual data
            virtualData: {
                slides: [],
            },
        }
    }
    componentDidMount() {
        const self = this;
        const swiper = new Swiper('.swiper-container', {
            loop: true,
            navigation: {
                nextEl: '.main-slider-next',
                prevEl: '.main-slider-prev',
            },
            virtual: {
                slides: self.state.slides,
                renderExternal(data) {
                    // assign virtual slides data
                    self.setState({
                        virtualData: data,
                    });
                }
            },
        });
    }
    render() {
        return (
        <div className="main-slider">
            <div className="swiper-container">
                <div className="swiper-wrapper">
                    {this.state.virtualData.slides.map((slide, index) => (
                        <div className="swiper-slide" key={index}>
                            <Slide slide={slide}></Slide>
                        </div>
                    ))}
                </div>
                <div className="main-slider-next"></div>
                <div className="main-slider-prev"></div>
            </div>
        </div>
        )
    }
}

export default Slider;