import React, { Component } from "react";
import "./style.css";
import fst from "./../../assets/images/pics/site1.jpg";
import snd from "./../../assets/images/pics/site2.jpg";
import thd from "./../../assets/images/pics/site3.jpg";

class LastWorks extends Component {
  render() {
    const list = [
      { id: 1, title: "Работа 1", link: "#", img: fst },
      { id: 2, title: "Работа 2", link: "#", img: snd },
      { id: 3, title: "Работа 3", link: "#", img: thd }
    ];

    return (
      <div className="section lastworks">
        <div className="container">
          <div class="section__title">Последние работы</div>
          <div class="lastworks-items">
            {list.map((el, index) => (
              <a href={el.link} className="lastworks-item">
                <img className="lastworks-item__img" src={el.img} />
              </a>
            ))}
          </div>
        </div>
      </div>
    );
  }
}

export default LastWorks;
