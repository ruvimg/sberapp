import React, { Component } from "react";
import "./style.css";
import fst from "./../../assets/images/pics/news1.jpg";
import snd from "./../../assets/images/pics/news2.jpg";

const NewsItem = function NewsItem(props) {
  return (
    <div className="news-item">
      <img className="news-item__img" src={props.data.img} />
      <div class="news-item__group_content">
        <div className="news-item__date">{props.data.date}</div>
        <div className="news-item__text">{props.data.text}</div>
        <a href={props.data.link} className="news-item__link">
          Подробнее
        </a>
      </div>
    </div>
  );
};

class News extends Component {
  render() {
    const list = [
      {
        id: 1,
        text:
          "Мы начинаем этот год с наших новых разроботок в области маркетинга. Ждем Вас на наших тренингах и лекциях",
        date: "1 января 2018",
        link: "#",
        img: fst
      },
      {
        id: 2,
        text:
          "Мы работаем, вы отдыхаете! Теперь мы предоставляем полный спектр услуг по привлечению клиентов!",
        date: "12 марта 2018",
        link: "#",
        img: snd
      }
    ];

    return (
      <div className="section news">
        <div className="container">
          <div class="section__title">Новости</div>
          <div class="news-items">
            {list.map((item, index) => (
              <NewsItem data={item} />
            ))}
          </div>
        </div>
      </div>
    );
  }
}

export default News;
