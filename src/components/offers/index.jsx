import React, { Component } from "react";
import "./style.css";
import comp from "./../../assets/images/icons/computer.png";
import nw2 from "./../../assets/images/icons/networking2.png";
import set from "./../../assets/images/icons/settings.png";
import nw from "./../../assets/images/icons/networking.png";
import lt from "./../../assets/images/icons/laptop.png";
import stup from "./../../assets/images/icons/startup.png";
const Item = function Item(props) {
  return (
    <div className="offer-item">
      <img className="offer-item__img" src={props.content.img} />
      <div className="offer-item__title">{props.content.title}</div>
      <div className="offer-item__text">{props.content.text}</div>
    </div>
  );
};

class Offers extends Component {
  render() {
    const list = [
      {
        id: 1,
        title: "создадим продающий сайт",
        text:
          "Нету сайта? Не беда. Наши специалисты разработают оптимизированный продающий сайт для Вашей компании в минимальные сроки.",
        img: comp
      },
      {
        id: 2,
        title: "выберем аудиторию",
        text:
          "Составим аудиторию Ваших потенциальных клиентов для качественных продаж.",
        img: nw2
      },
      {
        id: 3,
        title: "настроим статистику",
        text:
          "Настроим статистику сайта, будем анализироваться действия пользователей и улучшать функционал.",
        img: set
      },
      {
        id: 4,
        title: "разовьем соц. сети",
        text:
          "Для качественных продаж крайне необходимо вести активную деятельность в социалных сетях. Наши специалисты комплексно подойдут к вопросу привлечения клиентов через соц. сети.",
        img: nw
      },
      {
        id: 5,
        title: "минимизируем бюджет",
        text:
          "Главное не тратить деньги в пустую. Мы следим за тем, какая реклама дают максимальный результат и стремимся найти самые активные точки продаж для минимизации рекламного бюджета.",
        img: lt
      },
      {
        id: 6,
        title: "привлечем клиентов",
        text:
          "Наша цель - максимальное количество киентов для Ваших продаж. Мы работаем по всем направления в интернет-маркетинге для того, чтобы найти именно Ваших потенциальных клиентов!",
        img: stup
      }
    ];

    return (
      <div className="section offer">
        <div className="container">
          <div class="offer-items">
            {list.map((el, index) => (
              <Item content={el} />
            ))}
          </div>
        </div>
      </div>
    );
  }
}

export default Offers;
